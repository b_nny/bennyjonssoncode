﻿
namespace CodeExample.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // Implements application logic using the HelloWorldEntities context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public partial class HelloWorldDomainService : LinqToEntitiesDomainService<HelloWorldEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'Helloes' query.
        [Query(IsDefault = true)]
        public IQueryable<Hello> GetHelloes()
        {
            return this.ObjectContext.Helloes;
        }

        public void InsertHello(Hello hello)
        {
            if ((hello.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(hello, EntityState.Added);
            }
            else
            {
                this.ObjectContext.Helloes.AddObject(hello);
            }
        }

        public void UpdateHello(Hello currentHello)
        {
            this.ObjectContext.Helloes.AttachAsModified(currentHello, this.ChangeSet.GetOriginal(currentHello));
        }

        public void DeleteHello(Hello hello)
        {
            if ((hello.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(hello, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.Helloes.Attach(hello);
                this.ObjectContext.Helloes.DeleteObject(hello);
            }
        }
    }
}


