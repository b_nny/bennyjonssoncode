namespace CodeExample.Web
{
    using System;
    using System.ComponentModel.DataAnnotations;
    /// <summary>
    /// Default values using partial class
    /// </summary>
    public partial class Hello
    {
        public Hello()
        {
            // Set default value
            created = DateTime.Now;
        }
    }
}