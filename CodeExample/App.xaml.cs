﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using CodeExample.Localization;
using CodeExample.ViewModels;

namespace CodeExample
{
    public partial class App : Application
    {
        private HelloWorldMainPageViewModel _HelloWorldMainPageViewModel;

        public App()
        {

            this.Startup += this.Application_Startup;
            this.Exit += this.Application_Exit;
            this.UnhandledException += this.Application_UnhandledException;

            InitializeComponent();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {

            // Create an array of culture names.
            string[] cultureStrings = { "en-US", "sv-SE","de-DE" };
            // Get a random integer.
            Random rnd = new Random();
            int index = rnd.Next(0, cultureStrings.Length);
            // Set the current UI culture.
            string cultureString = cultureStrings[index];
            Thread.CurrentThread.CurrentCulture = new CultureInfo(cultureString);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureString);

            // Set the title in browser tab
            var document = HtmlPage.Document;
            String myTitle = string.Format("{0} ({1})", CodeExampleResource.HelloWorldCaption, cultureString);
            document.SetProperty("title", myTitle); // title should be in lower case.

            _HelloWorldMainPageViewModel = new HelloWorldMainPageViewModel();

            this.RootVisual = new HelloWorldMainPage(_HelloWorldMainPageViewModel);
            
        }

        private void Application_Exit(object sender, EventArgs e)
        {

        }
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            // If the app is running outside of the debugger then report the exception using
            // the browser's exception mechanism. On IE this will display it a yellow alert 
            // icon in the status bar and Firefox will display a script error.
            if (!System.Diagnostics.Debugger.IsAttached)
            {

                // NOTE: This will allow the application to continue running after an exception has been thrown
                // but not handled. 
                // For production applications this error handling should be replaced with something that will 
                // report the error to the website and stop the application.
                e.Handled = true;
                Deployment.Current.Dispatcher.BeginInvoke(delegate { ReportErrorToDOM(e); });
            }
        }
        private void ReportErrorToDOM(ApplicationUnhandledExceptionEventArgs e)
        {
            try
            {
                string errorMsg = e.ExceptionObject.Message + e.ExceptionObject.StackTrace;
                errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");

                System.Windows.Browser.HtmlPage.Window.Eval("throw new Error(\"Unhandled Error in Silverlight Application " + errorMsg + "\");");
            }
            catch (Exception)
            {
            }
        }
    }
}
