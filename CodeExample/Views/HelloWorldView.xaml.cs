﻿using System;
using System.Linq;
using Telerik.Windows.Controls;

namespace CodeExample.Views
{
    /// <summary>
    /// Inherit RadWindow for HelloWorld
    /// </summary>
    public partial class HelloWorldView : RadWindow
    {
        public HelloWorldView()
        {
            InitializeComponent();
        }
    }
}
