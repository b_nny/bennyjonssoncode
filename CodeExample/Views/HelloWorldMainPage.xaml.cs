﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace CodeExample
{
    public partial class HelloWorldMainPage : UserControl
    {
        /// <summary>
        /// Private to avoid creation without ViewModel
        /// </summary>
        private HelloWorldMainPage()
        {
            InitializeComponent();
            
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dataContext">The ViewModel</param>
        public HelloWorldMainPage(object dataContext)
        {
            InitializeComponent();
            this.DataContext = dataContext;
        }
    }
}
