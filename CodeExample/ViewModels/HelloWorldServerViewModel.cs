﻿using System;
using CodeExample.Extensions;
using CodeExample.Models;
using System.ServiceModel.DomainServices.Client;
using CodeExample.Web;

namespace CodeExample.ViewModels
{
    public class HelloWorldServerViewModel : AbstractHelloWorldViewModel
    {
        private readonly HelloRepository helloRepository = new HelloRepository();

        private Hello hello;
        public HelloWorldServerViewModel(String name)
            : base(CodeExample.Localization.CodeExampleResource.Server)
        {
            hello = new Hello() { name = name };
            // Insert the name in the database
            helloRepository.AddHallo(hello, OnSubmitCompleted);
            HelloName = name.Hello();
         }


        private void OnSubmitCompleted(SubmitOperation so)
        {
            if (!so.HasError)
            {
                // Show hello
                this.HelloName = hello.name.Hello();
            }
       	    
        }
    }
}
