﻿using System;
using CodeExample.Extensions;
namespace CodeExample.ViewModels
{
    /// <summary>
    /// Hello World Client ViewModel
    /// </summary>
    public class HelloWorldClientViewModel : AbstractHelloWorldViewModel
    {
        public HelloWorldClientViewModel(string name)
            : base( CodeExample.Localization.CodeExampleResource.Client)
        {
            HelloName = name.Hello();
        }

    }
}
