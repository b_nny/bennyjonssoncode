﻿using System;
using System.Linq.Expressions;
using Telerik.Windows.Controls;
using System.Collections.Generic;

namespace CodeExample.ViewModels
{
    /// <summary>
    /// Extending the telerik ViewModelBase with extra things that helps you writing better get, set and NotifyPropertyChange
    /// </summary>
    public abstract class ExtendedViewModelBase : ViewModelBase
    {
       
        // PRIVATE
        Dictionary<string, object> _propertyValues;
        // Get and Set help methods (To simplify the code for NotifyPropertyChange in each ViewModel)
        public ExtendedViewModelBase()
        {

            _propertyValues = new Dictionary<string, object>();
        }

        /// <summary>
        /// Smart get
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exp"></param>
        /// <returns></returns>
        public T Get<T>(Expression<Func<T>> exp)
        {
            return Get<T>((exp.Body as MemberExpression).Member.Name);
        }

        /// <summary>
        /// String property name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exp"></param>
        /// <returns></returns>
        public String GetPropertyName<T>(Expression<Func<T>> exp)
        {
            return (exp.Body as MemberExpression).Member.Name;
        }
        
        protected T Get<T>(string propertyName)
        {
            if (_propertyValues.ContainsKey(propertyName))
                return (T)_propertyValues[propertyName];

            return default(T);
        }

        /// <summary>
        /// Smart Set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exp"></param>
        /// <param name="propertyValue"></param>
        public void Set<T>(Expression<Func<T>> exp, T propertyValue)
        {
            Set<T>((exp.Body as MemberExpression).Member.Name, propertyValue);
        }

        protected void Set<T>(string propertyName, T propertyValue)
        {
            if (_propertyValues == null)
                _propertyValues = new Dictionary<string, object>();

            // The Dictionary will keep track of property values. They are removed when the value is equal to the defualt value of the property type
            if ((!_propertyValues.ContainsKey(propertyName) && propertyValue != null && !propertyValue.Equals(default(T))) || (_propertyValues.ContainsKey(propertyName) && (!_propertyValues[propertyName].Equals(propertyValue))))
            {
                if (_propertyValues.ContainsKey(propertyName) && (propertyValue == null || propertyValue.Equals(default(T))))
                    _propertyValues.Remove(propertyName);
                else
                    _propertyValues[propertyName] = propertyValue;

                OnPropertyChanged(propertyName);
            }
        }
    }
}
