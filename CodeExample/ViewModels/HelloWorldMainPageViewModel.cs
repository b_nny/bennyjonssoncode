﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using CodeExample.Views;

namespace CodeExample.ViewModels
{
    /// <summary>
    /// MainPage ViewModel
    /// </summary>
    public class HelloWorldMainPageViewModel : ExtendedViewModelBase
    {
        HelloWorldView helloWorldView;
        public DelegateCommand Server { get; private set; }
        public DelegateCommand Client { get; private set; }

        public String Name
        {
            get
            {
                return Get<String>(() => Name);
            }
            set
            {
                Set<String>(() => Name, value);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public HelloWorldMainPageViewModel()
        {
            this.Server = new DelegateCommand(OnServerExecute, CanServerExecute);
            this.Client = new DelegateCommand(OnClientExecute, CanClientExecute);
            helloWorldView = new HelloWorldView();
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            if (propertyName == GetPropertyName<String>(() => Name))
            {
                Server.InvalidateCanExecute();
                Client.InvalidateCanExecute();
            }

            base.OnPropertyChanged(propertyName);
        }

        private void OnClientExecute(object parameter)
        {
            // Use Client ViewModel
            helloWorldView.DataContext = new HelloWorldClientViewModel(Name);
            helloWorldView.ShowDialog();
        }

        private void OnServerExecute(object parameter)
        {
            // Use Server ViewModel
            helloWorldView.DataContext = new HelloWorldServerViewModel(Name);
            helloWorldView.ShowDialog();
        }

        private bool CanServerExecute(object obj)
        {
            return CanExecute();
        }

        private bool CanClientExecute(object obj)
        {
            return CanExecute();
        }

        private bool CanExecute()
        {
            // At least 1 letter
            return Name != null && Name.Length > 0;
        }
    }
}