﻿using System;

namespace CodeExample.ViewModels
{
    /// <summary>
    /// AbstractHelloWorldViewModel
    /// </summary>
    public abstract class AbstractHelloWorldViewModel : ExtendedViewModelBase
    {
        // Private to avoid creation without header
        private AbstractHelloWorldViewModel()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="header">Header</param>
        public AbstractHelloWorldViewModel(string header)
        {
            this.Header = header;
        }

        /// <summary>
        /// HelloName 
        /// </summary>
        public string HelloName
        {
            get
            {
                return Get<String>(() => HelloName);
            }
            set
            {
                Set<String>(() => HelloName, value);
            }
        }
        
        /// <summary>
        /// Header
        /// </summary>
        public string Header
        {
            get
            {
                return Get<String>(() => Header);
            }
            set
            {
                Set<String>(() => Header, value);
            }
        }
    }
}