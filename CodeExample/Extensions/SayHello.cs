﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace CodeExample.Extensions
{
    public static class SayHello
    {
        /// <summary>
        /// Hello world extension method
        /// </summary>
        /// <param name="s">Name</param>
        /// <returns>Hello, Name!</returns>
        public static string Hello(this string s)
        {
            return String.Format("{0}, {1}!",Localization.CodeExampleResource.Hello, s);
        }
    }
}
