﻿using System;
using CodeExample.Web;

namespace CodeExample.Models
{
    /// <summary>
    /// Model for Hello class
    /// </summary>
    public class HelloRepository : CodeExampleRepositoryBase<Hello>
    {
        public virtual void AddHallo(Hello hello, Action<System.ServiceModel.DomainServices.Client.SubmitOperation> action)
        {
            Service.Hellos.Add(hello);
            Service.SubmitChanges(action, null);
        }
    }
}
