﻿using System;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using CodeExample.Web;
using System.Linq;

namespace CodeExample.Models
{
    /// <summary>
    ///  A base class defining common functionality (such as the service reference) for all repository classes
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class CodeExampleRepositoryBase<T> : IDisposable
        where T : System.ServiceModel.DomainServices.Client.Entity
    {
        public HelloWorldDomainContext Service
        {
            get
            {
                return CodeExampleServiceContext.Service;
            }
        }

        protected void ProcessSingle(Action<T> completedAction, EntityQuery<T> query)
        {
            var loadOperation = Service.Load<T>(query);
            loadOperation.Completed += ((sender, e) =>
            {
                var operation = sender as LoadOperation<T>;
                if (operation != null)
                {
                    completedAction(operation.Entities.FirstOrDefault());
                }
            });
        }

        protected void ProcessCollection(Action<IEnumerable<T>> completedAction, EntityQuery<T> query)
        {
            var loadOperation = Service.Load<T>(query);
            loadOperation.Completed += ((sender, e) =>
            {
                var operation = sender as LoadOperation<T>;
                if (operation != null)
                {
                    completedAction(operation.Entities);
                }
            });
        }

        public virtual void Reject()
        {
            if (Service.HasChanges)
            {
                Service.RejectChanges();
            }
        }

        public virtual bool HasChanges()
        {
            return Service.HasChanges;
        }

        public virtual void Submit()
        {
            if (Service.HasChanges)
            {
                Service.SubmitChanges(SubmitOperationCompleted, null);
            }
        }

        protected void SubmitOperationCompleted(SubmitOperation operation)
        {
        }

        public void Dispose()
        {
        }
    }
}