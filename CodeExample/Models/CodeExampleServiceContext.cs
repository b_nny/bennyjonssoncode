﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CodeExample.Web;

namespace CodeExample.Models
{
    /// <summary>
    /// CodeExampleServiceContext
    /// </summary>
    public class CodeExampleServiceContext
    {
        // Must be static in order to get a single domain context, so that navigation properties are correctly handled
        private static HelloWorldDomainContext _service = new HelloWorldDomainContext();

        public static HelloWorldDomainContext Service
        {
            get
            {
                return _service;
            }
        }
    }
}

